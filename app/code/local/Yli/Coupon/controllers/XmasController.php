<?php

class Yli_Coupon_XmasController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        //$this->loadLayout();
        //$this->renderLayout();

        $rule_ids_string = "297,298,299,300,301,302,303,304,305,306,307,308"; // 在后台建个设置
        $rule_ids = explode(",",$rule_ids_string);

        if ( !Mage::getSingleton('customer/session')->isLoggedIn() )
        {
            $coupons_info = array();
            foreach ($rule_ids as $rule_id){
                $get_link = Mage::getBaseUrl().'coupon/get/?rule_id='.$rule_id;  

                $salesrule = Mage::getModel('salesrule/rule')->load( $rule_id );
                $coupons_info[] = array(
                    'rule_id'=>$salesrule->getRuleId(),
                    'name'=>$salesrule->getName(),
                    'discount_amount'=>$salesrule->getDiscountAmount(),
                    //'description'=>$salesrule->getDescription(),
                    'from_date'=>$salesrule->getFromDate(),
                    'to_date'=>$salesrule->getToDate(),
                    'is_active'=>$salesrule->getIsActive(),
                    'times_used'=>$salesrule->getTimesUsed(),
                    'store_today'=>now(),
                    'coupon_code'=>"",
                    'get_link'=>$get_link);
            }
           
            $result = json_encode($coupons_info);
            header("Content-Type:application/json; charset=UTF-8");
            echo($result);
            die();
        }
        

        
        $customer_id = Mage::getSingleton('customer/session')->getCustomerId();

        $coupons_info = array();
        foreach ($rule_ids as $rule_id){

            $coupon = Mage::getModel('salesrule/coupon')->getCollection()
                        ->addFieldToFilter('customer_id',$customer_id)
                        ->addFieldToFilter('rule_id',$rule_id);

            //var_dump($coupon);
            $coupon_code = "";

            if ( $coupon ){
                foreach ($coupon as $c){
                    $coupon_code = $c->getCode();
                    break;
                }
            }

            $get_link = Mage::getBaseUrl().'coupon/get/?rule_id='.$rule_id;  

            $salesrule = Mage::getModel('salesrule/rule')->load( $rule_id );
            $coupons_info[] = array(
                'rule_id'=>$salesrule->getRuleId(),
                'name'=>$salesrule->getName(),
                'discount_amount'=>$salesrule->getDiscountAmount(),
                //'description'=>$salesrule->getDescription(),
                'from_date'=>$salesrule->getFromDate(),
                'to_date'=>$salesrule->getToDate(),
                'is_active'=>$salesrule->getIsActive(),
                'times_used'=>$salesrule->getTimesUsed(),
                'store_today'=>now(),
                'coupon_code'=>$coupon_code,
                'get_link'=>$get_link);
        }
       
	    $result = json_encode($coupons_info);
	    header("Content-Type:application/json; charset=UTF-8");
	    echo($result);
	    die();
    }
}