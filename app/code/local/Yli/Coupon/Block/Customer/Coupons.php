<?php

// 为圣诞优惠券专题页面
class Yli_Coupon_Block_Customer_Coupons extends Mage_Core_Block_Template
{
    
    public function getCoupons()
    {
        $rule_ids_string = "190,191"; // 在后台建个设置
        $rule_ids = explode(",",$rule_ids_string);

        if ( !Mage::getSingleton('customer/session')->isLoggedIn() )
        {
            return 0;
        }
        
        $customer_id = Mage::getSingleton('customer/session')->getCustomerId();

        $coupons_info = array();
        foreach ($rule_ids as $rule_id){

            $coupon = Mage::getModel('salesrule/coupon')->getCollection()
                        ->addFieldToFilter('customer_id',$customer_id)
                        ->addFieldToFilter('rule_id',$rule_id);

            //var_dump($coupon);
            $coupon_code = 0;

            if ( $coupon ){
                foreach ($coupon as $c){
                    $coupon_code = $c->getCode();
                    break;
                }
            }

            $get_link = Mage::getBaseUrl().'coupon/get/?rule_id='.$rule_id;  

            $salesrule = Mage::getModel('salesrule/rule')->load( $rule_id );
            $coupons_info[] = array(
                'rule_id'=>$salesrule->getRuleId(),
                'name'=>$salesrule->getName(),
                //'description'=>$salesrule->getDescription(),
                'from_date'=>$salesrule->getFromDate(),
                'to_date'=>$salesrule->getToDate(),
                'is_active'=>$salesrule->getIsActive(),
                'times_used'=>$salesrule->getTimesUsed(),
                'store_today'=>now(),
                'coupon_code'=>$coupon_code,
                'get_link'=>$get_link);
        }
        
        return json_encode($coupons_info);
        
    }
    
}