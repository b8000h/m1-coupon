<?php

class Yli_Coupon_Block_Customer_Coupon extends Mage_Core_Block_Template
{
    private function _getCoupons()
    {
        $customer_id = Mage::getSingleton('customer/session')->getCustomerId();
        $coupons = Mage::getModel('salesrule/coupon')->getCollection()
                    ->addFieldToFilter('is_active',array('neq'=>0))
                    ->addFieldToFilter('customer_id',$customer_id);
        $coupons->getSelect()->joinLeft(
                array( 'salesrule' => $coupons->getTable('salesrule/rule')),
                'main_table.rule_id = salesrule.rule_id',
                array('name')
        );
        return $coupons;
    }

    public function getCoupons()
    {
        $coupons = $this->_getCoupons();
        if( empty($coupons) ){
            return 0;
        }

        $coupons_info = array();
        foreach( $coupons as $coupon )
        {
            $salesrule = Mage::getModel('salesrule/rule')->load( $coupon->getRuleId() );
            $coupons_info[] = array(
                'rule_id'=>$salesrule->getRuleId(),
                'name'=>$salesrule->getName(),
                'discount_amount'=>$salesrule->getDiscountAmount(),
                'description'=>$salesrule->getDescription(),
                'from_date'=>$salesrule->getFromDate(),
                'to_date'=>$salesrule->getToDate(),
                'is_active'=>$salesrule->getIsActive(),
                'times_used'=>$coupon->getTimesUsed(),
                'code'=>$coupon->getCode(),
                'store_today'=>now());
        }
        return json_encode($coupons_info);
    }

    // 返回 array，区别于上面的返回 json，备用
    public function getCouponArray()
    {
        $coupons = $this->_getCoupons();
        if( empty($coupons) ){
            return 0;
        }
        return $coupons;
    }
}