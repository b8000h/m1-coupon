<?php
/**
 *
 *
 * @category    Yli
 * @package     Yli_Coupon
 */


$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE `{$installer->getTable('salesrule/coupon')}` ADD `customer_id` INT(10) NOT NULL AFTER `rule_id`;");
$installer->endSetup();

/*
$installer = $this;


$installer->getConnection()->addColumn(
        $installer->getTable('salesrule/coupon'),
        'customer_id',
        array(
            'type'     => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'comment'  => 'Customer ID',
            'default'  => 0
        )
    );
*/